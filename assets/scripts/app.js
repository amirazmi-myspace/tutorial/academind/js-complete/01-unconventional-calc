const defaultResult = 0;
const logEntries = []
let currentResult = defaultResult;

function getInput() {
    return parseInt(userInput.value)
}

function createWriteOutput(operator, resultBeforeCalc, calcNumber) {
    const calcDesc = `${resultBeforeCalc} ${operator} ${calcNumber}`;
    outputResult(currentResult, calcDesc);
}

function writeToLog(operationIdentifier, prevResult, operationNumber, newResult) {
    const logEntry = {
        prevResult,
        operation: operationIdentifier,
        number: operationNumber,
        result: newResult
    }
    logEntries.push(logEntry)
    console.log(logEntries)
}

function calculateResult(calcType) {
    const enteredNumber = getInput();
    const initialResult = currentResult;
    let mathOperator;

    if (calcType !== 'ADD' && calcType !== 'SUB' && calcType !== 'MUL' && calcType !== 'DIV' || !enteredNumber) return
    
    if (calcType === 'ADD') {
        currentResult += enteredNumber;
        mathOperator = '+';
    } else if (calcType==='SUB'){
        currentResult -= enteredNumber;
        mathOperator = '-';
    } else if (calcType === 'MUL') {
        currentResult *= enteredNumber;
        mathOperator = 'x';
    } else if (calcType === 'DIV') {
        currentResult /= enteredNumber;
        mathOperator = '/';
    }


    createWriteOutput(mathOperator, initialResult, enteredNumber);
    writeToLog(calcType, initialResult, enteredNumber, currentResult)
}

addBtn.addEventListener('click', () => calculateResult('ADD'));
subtractBtn.addEventListener('click', () => calculateResult('SUB'));
multiplyBtn.addEventListener('click', () => calculateResult('MUL'));
divideBtn.addEventListener('click', () => calculateResult('DIV'));
